<?php

namespace Money;


class Money {

  public function __construct(
    public readonly float $value,
    public readonly CurrencyEnum $currency,
  ) {
  }
}
