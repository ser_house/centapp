<?php

namespace Money;

enum CurrencyEnum: string {
  case RUB = 'RUB';
  case EUR = 'EUR';
}
