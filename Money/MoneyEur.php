<?php

namespace Money;

class MoneyEur extends Money {

  public function __construct(float $value = 0.0) {
    parent::__construct($value, CurrencyEnum::EUR);
  }
}
