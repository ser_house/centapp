<?php

namespace BankServices;

use Payment\Payment;

interface IBankService {

  /**
   * @param Payment $payment
   *
   * @return void
   */
  public function send(Payment $payment): void;
}
