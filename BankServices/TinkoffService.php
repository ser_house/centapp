<?php

namespace BankServices;

use BankServices\Exceptions\AmountNotSupportedException;
use Money\CurrencyEnum;
use Payment\Payment;

class TinkoffService implements IBankService {

  // Нет смысла заводить для такого ограничения админский UI, достаточно в конфигах держать
  private const MIN_PAYMENT_AMOUNT_IN_RUB = 15000.0;

  /**
   * @inheritDoc
   */
  public function send(Payment $payment): void {
    if (CurrencyEnum::RUB === $payment->amount->currency && self::MIN_PAYMENT_AMOUNT_IN_RUB > $payment->amount->value) {
      throw new AmountNotSupportedException("Amount: {$payment->amount->value}, min: " . self::MIN_PAYMENT_AMOUNT_IN_RUB);
    }

    // ...send
  }

}
