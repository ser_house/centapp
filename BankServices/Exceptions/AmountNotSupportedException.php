<?php

namespace BankServices\Exceptions;

use DomainException;

class AmountNotSupportedException extends DomainException {

}
