<?php

namespace BankServices\Exceptions;

use DomainException;

class UnknownBankException extends DomainException {

}
