<?php

namespace BankServices;

enum BankEnum : string {
  case sber = 'sber';
  case tinkoff = 'tinkoff';
}
