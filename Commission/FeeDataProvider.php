<?php

namespace Commission;

use Money\CurrencyEnum;
use Money\MoneyEur;
use Money\MoneyRub;
use Payment\PayTypeEnum;

/**
 * Можно еще больше спрятать детали, если добавить интерфейс IFeeDataProvider::get
 * с этой реализацией и другими (для базы, например).
 * Методы cardFee и qiwiFee похожи на напрашивающиеся в вынос в фабрику,
 * но только похожи, поскольку этот класс просто имитация запроса куда-то
 * для получения данных по неким критериям.
 */
class FeeDataProvider {

  /**
   * @param PayTypeEnum $payType
   * @param CurrencyEnum $currency
   * @param float $amount
   *
   * @return Fee
   */
  public function get(PayTypeEnum $payType, CurrencyEnum $currency, float $amount): Fee {
    $fees = $this->fees();
    if (!isset($fees[$payType->value][$currency->value]) || $amount < 1.0) {
      return new Fee();
    }

    switch($payType) {
      case PayTypeEnum::card:
        return $this->cardFee($currency, $amount, $fees);

      case PayTypeEnum::phone:
        return $this->qiwiFee($currency, $amount, $fees);

      default:
        return new Fee();
    }
  }

  /**
   * @param CurrencyEnum $currency
   * @param float $amount
   * @param array $fees
   *
   * @return Fee
   */
  private function cardFee(CurrencyEnum $currency, float $amount, array $fees): Fee {
    if ($amount > 10000.0) {
      return new Fee();
    }

    $pay_currency_data = $fees[PayTypeEnum::card->value][$currency->value];

    switch($currency) {
      case CurrencyEnum::RUB:
        if ($amount < 1000.1) {
          return $pay_currency_data['<1000'];
        }
        return $pay_currency_data['<10000'];

      case CurrencyEnum::EUR:
        return $pay_currency_data['<10000'];

      default:
        return new Fee();
    }
  }

  /**
   * @param CurrencyEnum $currency
   * @param float $amount
   * @param array $fees
   *
   * @return Fee
   */
  private function qiwiFee(CurrencyEnum $currency, float $amount, array $fees): Fee {
    if ($amount > 75000.0) {
      return new Fee();
    }

    $pay_currency_data = $fees[PayTypeEnum::card->value][$currency->value];

    switch($currency) {
      case CurrencyEnum::RUB:
        return $pay_currency_data['<75000'];

      default:
        return new Fee();
    }
  }

  /**
   * @return Fee[][][]
   */
  private function fees(): array {
    return [
      PayTypeEnum::card->value => [
        CurrencyEnum::RUB->value => [
          '<1000' => new Fee(4, new MoneyRub(1.0), new MoneyRub(3.0)),
          '<10000' => new Fee(3, new MoneyRub(1.0), new MoneyRub(3.0)),
        ],
        CurrencyEnum::EUR->value => [
          '<10000' => new Fee(7, new MoneyEur(1.0), new MoneyEur(4.0)),
        ],
      ],
      PayTypeEnum::phone->value => [
        CurrencyEnum::RUB->value => [
          '<75000' => new Fee(5, new MoneyRub(), new MoneyRub(3.0)),
        ],
      ],
    ];
  }
}
