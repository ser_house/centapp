<?php

namespace Commission;

use Money\CurrencyEnum;
use Money\Money;

class Fee {

  public function __construct(
    public readonly int $percent = 0,
    public readonly Money $fix = new Money(0.0, CurrencyEnum::RUB),
    public readonly Money $min = new Money(0.0, CurrencyEnum::RUB),
  ) {
  }
}
