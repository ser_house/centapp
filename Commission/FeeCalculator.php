<?php

namespace Commission;
use Money\Money;

/**
 * Если в системе потребуется использовать одновременно разные расчеты
 * (например, max для одних случаев и min для других),
 * то преобразовать в стратегию с реализацией IFeeCalculator
 * и переименованием этого класса в FeeCalculatorByMax (например).
 */
class FeeCalculator {

  /**
   * @param float $amount
   * @param Fee $fee
   *
   * @return Money
   */
  public function calc(float $amount, Fee $fee): Money {
    $percent_value = $amount * ($fee->percent / 100);
    $value = max($percent_value + $fee->fix->value, $fee->min->value);
    return new Money($value, $fee->fix->currency);
  }
}
