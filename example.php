<?php


use BankServices\BankEnum;
use BankServices\Exceptions\AmountNotSupportedException;
use BankServices\Exceptions\UnknownBankException;
use Commission\FeeCalculator;
use Commission\FeeDataProvider;
use Factories\Factory;
use Money\CurrencyEnum;
use Money\Money;
use Payment\Exceptions\PaymentTypeNotSupportedException;
use Payment\Exceptions\UnknownPaymentTypeException;
use Payment\PaymentAmountCalculatorFeeSeller;
use Payment\PayTypeEnum;

$currency = CurrencyEnum::from('RUB');
$amount = 15300.0;
$card_num = '1111';
$payType = PayTypeEnum::card;
$bank = BankEnum::sber;

$factory = new Factory();
$feeDataProvider = new FeeDataProvider();
$feeCalculator = new FeeCalculator();
$paymentAmountCalculator = new PaymentAmountCalculatorFeeSeller();

try {
  $byTypeFactory = $factory->getFactoryByPaymentType($payType, $card_num);

  $bankService = $byTypeFactory->buildBankService($bank);

  $payment = $byTypeFactory->buildPayment(new Money($amount, $currency));

  $fee = $feeDataProvider->get($payType, $currency, $payment->amount->value);
  $feeAmount = $feeCalculator->calc($payment->amount->value, $fee);
  $amountWithFee = $paymentAmountCalculator->calc($payment->amount, $feeAmount);
  $calculatedPayment = $byTypeFactory->buildPayment($amountWithFee);

  $bankService->send($calculatedPayment);
}
catch(Throwable $e){
  // 2 варианта:
  //   пишем в лог прямо на месте возникновения исключения
  //     плюсы - есть все данные под рукой, подсистема логирования тоже под рукой
  //             (если захотим логировать не только ошибки-исключения, а и что вообще происходит без ES)
  //     минусы - по всей системе раскидано логирование (если мы не АОП используем)
  //   ловим исключения в одном месте и там и пишем в лог
  //     плюсы - одна точка для логов
  //     минусы - все нужные для логов данные надо передать по всей цепочке
  // ну и формируем сообщение для пользователя, что-нибудь вроде
  // "Ой, что-то пошло не так, обратитесь туда-то, номер ошибки такой-то",
  // где номер ошибки - идентификатор для поиска в логах
  switch(true) {
    case $e instanceof UnknownPaymentTypeException:
    case $e instanceof UnknownBankException:
    case $e instanceof PaymentTypeNotSupportedException:
    case $e instanceof AmountNotSupportedException:
  }
}
