<?php

namespace Payment;

use Money\Money;

class PaymentAmountCalculatorFeeSeller implements IPaymentAmountCalculator {

  /**
   * @inheritDoc
   */
  public function calc(Money $amount, Money $feeAmount): Money {
    return $amount;
  }
}
