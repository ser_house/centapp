<?php

namespace Payment;

enum PayTypeEnum : string {
  case card = 'card';
  case phone = 'phone';
}
