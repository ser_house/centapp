<?php

namespace Payment;

use Money\Money;

class PaymentAmountCalculatorFeeBuyer implements IPaymentAmountCalculator {

  /**
   * @inheritDoc
   */
  public function calc(Money $amount, Money $feeAmount): Money {
    return new Money($amount->value + $feeAmount->value, $amount->currency);
  }
}
