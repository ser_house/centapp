<?php

namespace Payment;

use Money\Money;

class PaymentQiwi extends Payment {

  protected const PAY_TYPE = PayTypeEnum::phone;

  public function __construct(Money $amount, public readonly string $phone) {
    parent::__construct($amount);
  }
}
