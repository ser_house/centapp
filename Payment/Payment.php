<?php

namespace Payment;

use Money\Money;

abstract class Payment {
  protected const PAY_TYPE = null;

  public function __construct(public readonly Money $amount) {
  }
}
