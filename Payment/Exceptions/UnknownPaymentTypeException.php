<?php

namespace Payment\Exceptions;

use DomainException;

class UnknownPaymentTypeException extends DomainException {

}
