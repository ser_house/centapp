<?php

namespace Payment\Exceptions;

use DomainException;

class PaymentTypeNotSupportedException extends DomainException {

}
