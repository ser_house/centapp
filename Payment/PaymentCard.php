<?php

namespace Payment;

use Money\Money;

class PaymentCard extends Payment {

  protected const PAY_TYPE = PayTypeEnum::card;

  public function __construct(Money $amount, public readonly string $card_num) {
    parent::__construct($amount);
  }
}
