<?php

namespace Payment;

use Money\Money;

interface IPaymentAmountCalculator {

  /**
   * Возвращает сумму для оплаты покупателем.
   *
   * @param Money $amount сумма платежа, запрошенная продавцом
   * @param Money $feeAmount сумма комиссии
   *
   * @return Money
   */
  public function calc(Money $amount, Money $feeAmount): Money;
}
