<?php

namespace Factories;

use BankServices\BankEnum;
use BankServices\IBankService;
use Money\Money;
use Payment\Payment;

interface IByTypeFactory {

  /**
   * @param Money $amount
   *
   * @return Payment
   */
  public function buildPayment(Money $amount): Payment;

  /**
   * @param BankEnum $bank
   *
   * @return IBankService
   */
  public function buildBankService(BankEnum $bank): IBankService;
}
