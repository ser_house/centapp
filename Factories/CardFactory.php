<?php

namespace Factories;

use BankServices\BankEnum;
use BankServices\Exceptions\UnknownBankException;
use BankServices\IBankService;
use BankServices\SberService;
use BankServices\TinkoffService;
use Money\Money;
use Payment\PaymentCard;

class CardFactory implements IByTypeFactory {

  public function __construct(public readonly string $card_num) {
  }

  /**
   * @inheritDoc
   */
  public function buildPayment(Money $amount): PaymentCard {
    return new PaymentCard($amount, $this->card_num);
  }

  /**
   * @inheritDoc
   */
  public function buildBankService(BankEnum $bank): IBankService {
    return match($bank) {
      BankEnum::sber => new SberService(),
      BankEnum::tinkoff => new TinkoffService(),
      default => throw new UnknownBankException("$bank->name: $bank->value"),
    };
  }

}
