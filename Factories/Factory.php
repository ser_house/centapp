<?php

namespace Factories;

use Payment\Exceptions\UnknownPaymentTypeException;
use Payment\PayTypeEnum;

class Factory {

  public function getFactoryByPaymentType(PayTypeEnum $payType, string $pay_target_identifier): IByTypeFactory {
    return match($payType) {
      PayTypeEnum::card => new CardFactory($pay_target_identifier),
      PayTypeEnum::phone => new PhoneFactory($pay_target_identifier),
      default => throw new UnknownPaymentTypeException("$payType->name: $payType->value"),
    };
  }
}
