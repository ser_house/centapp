<?php

namespace Factories;

use BankServices\BankEnum;
use BankServices\Exceptions\UnknownBankException;
use BankServices\IBankService;
use BankServices\SberService;
use Payment\Exceptions\PaymentTypeNotSupportedException;
use Money\Money;
use Payment\PaymentQiwi;

class PhoneFactory implements IByTypeFactory {

  public function __construct(public readonly string $phone) {
  }

  /**
   * @inheritDoc
   */
  public function buildPayment(Money $amount): PaymentQiwi {
    return new PaymentQiwi($amount, $this->phone);
  }

  /**
   * @inheritDoc
   */
  public function buildBankService(BankEnum $bank): IBankService {
    return match($bank) {
      BankEnum::sber => new SberService(),
      BankEnum::tinkoff => throw new PaymentTypeNotSupportedException(),
      default => throw new UnknownBankException("$bank->name: $bank->value"),
    };
  }
}
